package tests;

import org.junit.After;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by danny on 3/11/16.
 */
public class IndexReaderTokenTest extends BaseIndexReaderTest {

    @Test
    public void testGetTokenCollectionFrequency() throws Exception {

        testCollectionFrequency("chewy", 4);
        testCollectionFrequency("Irish", 4);
        testCollectionFrequency("coffee", 13);
        testCollectionFrequency("rtrtr", 0);

    }

    @Test
    public void testGetTokenFrequency() throws Exception {

        testFrequency("chewy", 4);
        testFrequency("Irish", 4);
        testFrequency("coffee", 5);
        testFrequency("rtrtr", 0);

    }


    @Test
    public void testGetReviewsWithToken1() throws Exception {
        List<Integer> lis = new ArrayList<>();
        lis.add(1);
        lis.add(1);
        testReviewsToken("vitality", lis);
    }

    @Test
    public void testGetReviewsWithToken4() throws Exception {
        List<Integer> lis = new ArrayList<>();
        testReviewsToken(" ", lis);
    }

    @Test
    public void testGetReviewsWithToken2() throws Exception {
        List<Integer> lis = new ArrayList<>();
        lis.add(10);
        lis.add(1);
        lis.add(41);
        lis.add(1);
        lis.add(43);
        lis.add(1);
        lis.add(50);
        lis.add(1);
        testReviewsToken("healthy", lis);
    }

    @Test
    public void testGetReviewsWithToken3() throws Exception {
        List<Integer> lis = new ArrayList<>();
        lis.add(2);
        lis.add(2);
        lis.add(53);
        lis.add(5);
        testReviewsToken("Peanuts", lis);
    }

    @Test
    public void testNumberOfTokens() {

        int expected = 6903;
        int actual = indexReader.getTokenSizeOfReviews();

        assertEquals(expected, actual);

    }

    private void testReviewsToken(String token, List<Integer> expected) {

        Enumeration<Integer> reviewsWithToken = indexReader.getReviewsWithToken(token);
        ArrayList<Integer> actual = Collections.list(reviewsWithToken);
        assertEquals(expected, actual);

    }

    private void testCollectionFrequency(String token, int expected) {
        int actual = indexReader.getTokenCollectionFrequency(token);
        assertEquals(expected, actual);

    }

    private void testFrequency(String token, int expected) {
        int actual = indexReader.getTokenFrequency(token);
        assertEquals(String.format("Wrong count for %s", token), expected, actual);
    }

    @Override
    protected String getInputFile() {

        return "/home/danny/projects/university/webdata/ex1/data/100.txt";

    }
}