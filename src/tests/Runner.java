package tests;

import webdata.IndexReader;
import webdata.IndexWriter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;


/**
 * Created by danny on 4/5/16.
 */
public class Runner {


    public static void main(String[] args) throws IOException {


        String[] inputs = {"data/movies/1000.txt", "data/movies/10000.txt", "data/movies/100000.txt", "data/movies/1000000.txt", "data/movies/all.txt"};
        String[] outputs = {"output/1000", "output/10000", "output/100000", "output/1000000", "output/all"};

        for (int i = 0; i < 5; i++) {

            String inputPath = inputs[i];
            String outputPath = outputs[i];

            System.out.printf("Input file: %s%n", inputPath);

            run(inputPath, outputPath);

            System.out.println();
            System.out.println();
        }


    }

    private static void run(String inputPath, String outputPath) throws IOException {
        long start = System.currentTimeMillis();
        IndexWriter iw = new IndexWriter();
        iw.write(inputPath, outputPath);
        long after = System.currentTimeMillis();

        System.out.printf("Time for writing the index is %d milliseconds %n", after - start);

        File file = new File(outputPath);

        BufferedWriter writer = new BufferedWriter(new FileWriter("tt.rrr.r"));

        System.out.printf("the total size of the directory is %d bytes %n", folderSize(file));

        String[] randomWords = {"sausage", "blubber", "pencil", "cloud", "moon", "water", "computer", "school", "network", "hammer", "walking", "violently", "mediocre", "literature", "chair", "two", "window", "cords", "musical", "zebra", "xylophone", "penguin", "home", "dog", "final", "ink", "teacher", "fun", "website", "banana", "uncle", "softly", "mega", "ten", "awesome", "attatch", "blue", "internet", "bottle", "tight", "zone", "tomato", "prison", "hydro", "cleaning", "telivision", "send", "frog", "cup", "book", "zooming", "falling", "evily", "gamer", "lid", "juice", "moniter", "captain", "bonding", "loudly", "thudding", "guitar", "shaving", "hair", "soccer", "water", "racket", "table", "late", "media", "desktop", "flipper", "club", "flying", "smooth", "monster", "purple", "guardian", "bold", "hyperlink", "presentation", "world", "national", "comment", "element", "magic", "lion", "sand", "crust", "toast", "jam", "hunter", "forest", "foraging", "silently", "tawesomated", "joshing", "pong", "RANDOM", "WORD"};

        IndexReader reader = new IndexReader(outputPath);

        long middle = System.nanoTime();
        for (String randomWord : randomWords) {
            reader.getReviewsWithToken(randomWord);
        }


        long afterReviews = System.nanoTime();

        for (String randomWord : randomWords) {
            Enumeration<Integer> reviewsWithToken = reader.getReviewsWithToken(randomWord);

            writer.write(randomWord + " : ");

            while (reviewsWithToken.hasMoreElements()) {
                writer.write(String.format("(%d, %d), ", reviewsWithToken.nextElement(), reviewsWithToken.nextElement()));
            }

            writer.write("\n");
        }

        long end = System.nanoTime();

        System.out.printf("time for GetReviewsWithToken %d nanoseconds%n", afterReviews - middle);
        System.out.printf("time for getTokenFrequency %d nanoseconds%n", end - afterReviews);

        //        iw.removeIndex(outputPath);
    }

    private static long folderSize(File directory) {
        long length = 0;
        for (File file : directory.listFiles()) {
            if (file.isFile())
                length += file.length();
            else
                length += folderSize(file);
        }
        return length;
    }
}
