package tests;

import org.junit.After;
import org.junit.Before;
import webdata.IndexReader;
import webdata.IndexWriter;

/**
 * Created by danny on 3/15/16.
 */
public abstract class BaseIndexReaderTest {

    private static final String INDEX_OUTPUT = "/home/danny/projects/university/webdata/ex1/output/";

    protected IndexWriter writer;

    protected IndexReader indexReader;

    @Before
    public void setUp() throws Exception {

        writer = new IndexWriter();

        writer.write(getInputFile(), INDEX_OUTPUT);

        indexReader = new IndexReader(INDEX_OUTPUT);
    }

    protected abstract String getInputFile();

    @After
    public void tearDown() throws Exception {

        writer.removeIndex(INDEX_OUTPUT);

    }
}
