package tests;

import org.junit.Assert;
import org.junit.Test;
import webdata.ExternalSort;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.file.Files;

/**
 * Created by danny on 4/5/16.
 */
public class ExternalSortTest {

    @Test
    public void testSort() throws Exception {

        File tmp1 = new File("tmp1");
        tmp1.delete();
        File name = new File("name");
        Files.copy(name.toPath(), tmp1.toPath());

        long sizeBefore = name.length() / 8;

        ExternalSort externalSort = new ExternalSort(new RandomAccessFile(tmp1, "rw"), "here");
        RandomAccessFile accessFile = externalSort.sort();

        accessFile.seek(0);
        long sizeAfter = (accessFile.length() / 8);

        Assert.assertEquals(sizeBefore, sizeAfter);

//        int last = -1;
//
//        for (int j = 0; j < sizeAfter; j++) {
//
//            int i1 = accessFile.readInt();
//            accessFile.readInt();
//
//            if (last > i1) {
//                Assert.fail();
//            }
//
//            last = i1;
//        }

        externalSort.close();
    }
}