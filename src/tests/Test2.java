package tests;

import java.io.*;
import java.util.Scanner;

/**
 * Created by danny on 4/19/16.
 */
@SuppressWarnings("Duplicates")
public class Test2 {

    public static void main(String[] args) throws IOException {

        writeFileDebug(new DataInputStream(new FileInputStream("pairs.txt")), 100);

    }

    private static void writeFileDebug(DataInputStream readFile, int i) throws IOException {

        File file = new File("tmpw");

        FileWriter outputStream = new FileWriter(file);

//
//        int c = (int) (readFile.length() / 8);
//
//        System.out.println(c);

        while (readFile.available() > 0) {

            int i1 = readFile.readInt();
            int i2 = readFile.readInt();

            if (i1 < 0) {
                outputStream.write("$%$%$%$%$%");
                break;
            }

            outputStream.write(String.valueOf(i1));
            outputStream.write("-");
            outputStream.write(String.valueOf(i2));
            outputStream.write("\n");


        }

        outputStream.close();


    }
}
