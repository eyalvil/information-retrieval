package tests;

import webdata.IndexReader;
import webdata.IndexWriter;
import webdata.ReviewSearch;

import java.util.*;

/**
 * Created by danny on 5/24/16.
 */
public class IndexSearch {

    private static ReviewSearch reviewSearch;

    public static void main(String[] args) {

//
        IndexWriter iw = new IndexWriter();
        iw.write("data/100.txt", "output/100");

        IndexReader indexReader = new IndexReader("output/100");

        reviewSearch = new ReviewSearch(indexReader);

        searchResults("love cand");
//        searchResults("CaT");
//        searchResults("catcatcat");
//        searchResults("");
//        searchResults("cat catcat");
//        searchResults("the mammal");
//        searchResults("Tig Tag");
//        searchResults("John Terry Captain Leader Legend");
//        searchResults("Santi Cazorla");
//        searchResults("Ozil is SHIT");
//        searchResults("Hello world");
//        searchResults("Dog food");


    }

    public static void searchResults(String query) {

        String[] querySplited = query.split(" ");

        System.out.printf("Query: ");
        System.out.println(Arrays.toString(querySplited));

//        Enumeration<Integer> searchResult = reviewSearch.vectorSpaceSearch(Collections.enumeration(Arrays.asList(querySplited)), 10);
//        System.out.println(Collections.list(searchResult));

        Enumeration<Integer> searchResult = reviewSearch.languageModelSearch(Collections.enumeration(Arrays.asList(querySplited)), 0.5, 10);
        System.out.println(Collections.list(searchResult));
//
//        Collection<String> productSearch = reviewSearch.productSearch(Collections.enumeration(Arrays.asList(querySplited)), 10);
//        System.out.println(productSearch);

        System.out.println();
    }
}
