package tests;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by danny on 3/11/16.
 */
public class IndexReaderProductTest extends BaseIndexReaderTest {


    @Test
    public void testGetProductReviews1() throws Exception {

        List<Integer> expectedProductReviews = new ArrayList<>();
        expectedProductReviews.add(1);

        testProductReview("B001E4KFG0", expectedProductReviews);

    }

    @Test
    public void testGetProductReviews2() throws Exception {

        List<Integer> expectedProductReviews = new ArrayList<>();
        expectedProductReviews.add(3);

        testProductReview("B000LQOCH0", expectedProductReviews);

    }

    @Test
    public void testGetProductReviews3() throws Exception {

        List<Integer> expectedProductReviews = new ArrayList<>();
        expectedProductReviews.add(5);
        expectedProductReviews.add(6);
        expectedProductReviews.add(7);
        expectedProductReviews.add(8);

        testProductReview("B006K2ZZ7K", expectedProductReviews);
    }

    @Test
    public void testGetProductReviews4() throws Exception {

        List<Integer> expectedProductReviews = new ArrayList<>();

        testProductReview(" ", expectedProductReviews);
    }

    public void testProductReview(String productId, List<Integer> expectedProductReviews) {


        Enumeration<Integer> productReviews = indexReader.getProductReviews(productId);
        List<Integer> actualProductReviews = Collections.list(productReviews);

        Assert.assertEquals(expectedProductReviews, actualProductReviews);

    }

    @Override
    protected String getInputFile() {
        return "/home/danny/projects/university/webdata/ex1/data/100.txt";
    }
}