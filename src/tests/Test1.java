package tests;

import webdata.IndexReader;
import webdata.IndexWriter;

import java.io.IOException;

/**
 * Created by danny on 3/18/16.
 */
public class Test1 {

    public static void main(String[] args) throws IOException {

        IndexWriter writer = new IndexWriter();

        writer.write("data/1000.txt", "outputs1/");

        IndexReader reader = new IndexReader("outputs1/");

        for (int i = 0; i <= 1000; i++) {

            reader.getProductId(i);

            System.out.printf("Review: %d, prod: %s num: %d dum: %d score: %d len: %d%n", i, reader.getProductId(i), reader.getReviewHelpfulnessNumerator(i)
                    , reader.getReviewHelpfulnessDenominator(i), reader.getReviewScore(i), reader.getReviewLength(i));
        }


    }
}
