package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import webdata.utils.GroupVarintCompression;

import java.util.Vector;

import static org.junit.Assert.assertEquals;

/**
 * Created by danny on 3/18/16.
 */
public class GroupVarintCompressionTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testCompress1() throws Exception {

        Vector<Integer> vec = new Vector<>();
        vec.add(1);
        vec.add(16);
        vec.add(527);
        vec.add(131598);

        Vector<Byte> actual = GroupVarintCompression.compress(vec);

        Vector<Byte> expected = new Vector<>();
        expected.add((byte) 0b00000110);
        expected.add((byte) 0b00000001);
        expected.add((byte) 0b00001111);
        expected.add((byte) 0b00000001);
        expected.add((byte) 0b11111111);
        expected.add((byte) 0b00000001);
        expected.add((byte) 0b11111111);
        expected.add((byte) 0b11111111);


        assertEquals("Wrong compression", expected, actual);

    }

    @Test
    public void testCompress2() throws Exception {

        Vector<Integer> vec = new Vector<>();
        vec.add(1);
        vec.add(16);
        vec.add(527);
        vec.add(131598);
        vec.add(131599);
        vec.add(131600);

        Vector<Byte> actual = GroupVarintCompression.compress(vec);

        Vector<Byte> expected = new Vector<>();
        expected.add((byte) 0b00000110);
        expected.add((byte) 0b00000001);
        expected.add((byte) 0b00001111);
        expected.add((byte) 0b00000001);
        expected.add((byte) 0b11111111);
        expected.add((byte) 0b00000001);
        expected.add((byte) 0b11111111);
        expected.add((byte) 0b11111111);
        expected.add((byte) 0b0);
        expected.add((byte) 0b1);
        expected.add((byte) 0b1);


        assertEquals("Wrong compression", expected, actual);

    }


    @Test
    public void testDecompress() throws Exception {


        Vector<Integer> expected = new Vector<>();
        expected.add(1);
        expected.add(16);
        expected.add(527);
        expected.add(131598);


        Vector<Byte> vec = new Vector<>();
        vec.add((byte) 0b00000110);
        vec.add((byte) 0b00000001);
        vec.add((byte) 0b00001111);
        vec.add((byte) 0b00000001);
        vec.add((byte) 0b11111111);
        vec.add((byte) 0b00000001);
        vec.add((byte) 0b11111111);
        vec.add((byte) 0b11111111);


        Byte[] bytes = vec.toArray(new Byte[vec.size()]);
        byte[] b = new byte[vec.size()];

        for (int i = 0, bytesLength = bytes.length; i < bytesLength; i++) {
            Byte aByte = bytes[i];
            b[i] = aByte;
        }


        Vector<Integer> actual = GroupVarintCompression.decompress(b);

        assertEquals("Wrong compression", expected, actual);

    }

    @Test
    public void testDecompress2() throws Exception {


        Vector<Integer> expected = new Vector<>();
        expected.add(1);
        expected.add(16);
        expected.add(527);
        expected.add(131598);
        expected.add(131599);
        expected.add(131600);
        expected.add(131601);


        Vector<Byte> vec = new Vector<>();
        vec.add((byte) 0b00000110);
        vec.add((byte) 0b00000001);
        vec.add((byte) 0b00001111);
        vec.add((byte) 0b00000001);
        vec.add((byte) 0b11111111);
        vec.add((byte) 0b00000001);
        vec.add((byte) 0b11111111);
        vec.add((byte) 0b11111111);
        vec.add((byte) 0b0);
        vec.add((byte) 0b1);
        vec.add((byte) 0b1);
        vec.add((byte) 0b1);


        Byte[] bytes = vec.toArray(new Byte[vec.size()]);
        byte[] b = new byte[vec.size()];

        for (int i = 0, bytesLength = bytes.length; i < bytesLength; i++) {
            Byte aByte = bytes[i];
            b[i] = aByte;
        }


        Vector<Integer> actual = GroupVarintCompression.decompress(b);

        assertEquals("Wrong compression", expected, actual);

    }

    @Test
    public void testDecompress3() throws Exception {


        Vector<Integer> expected = new Vector<>();
        expected.add(402);
        expected.add(644);
        expected.add(738);
        expected.add(743);
        expected.add(823);
        expected.add(907);
        expected.add(909);
        expected.add(968);
        expected.add(997);
        expected.add(1352);
        expected.add(1629);
        expected.add(1739);
        expected.add(1872);
        expected.add(1888);
        expected.add(1986);
        expected.add(2125);
        expected.add(2297);
        expected.add(2304);
        expected.add(2305);
        expected.add(2320);
        expected.add(2327);
        expected.add(2328);
        expected.add(2345);
        expected.add(2349);
        expected.add(2990);
        expected.add(3003);
        expected.add(3004);
        expected.add(3012);
        expected.add(3094);
        expected.add(3407);
        expected.add(3484);
        expected.add(3592);
        expected.add(3628);
        expected.add(3647);
        expected.add(3685);
        expected.add(3724);
        expected.add(4338);
        expected.add(4400);
        expected.add(4444);
        expected.add(4760);
        expected.add(4959);
        expected.add(5383);
        expected.add(5488);
        expected.add(5517);
        expected.add(6019);
        expected.add(6187);
        expected.add(6230);
        expected.add(6587);
        expected.add(6615);
        expected.add(7127);
        expected.add(7384);
        expected.add(7799);
        expected.add(8866);
        expected.add(9635);
        expected.add(9861);

        System.out.println(GroupVarintCompression.compress(expected));

        Vector<Byte> vec = new Vector<>();
        vec.add((byte) 0b1000000);
        vec.add((byte) 0b1);
        vec.add((byte) -0b1101110);
        vec.add((byte) -0b1110);
        vec.add((byte) 0b1011110);
        vec.add((byte) 0b101);
        vec.add((byte) 0b0);
        vec.add((byte) 0b1010000);
        vec.add((byte) 0b1010100);
        vec.add((byte) 0b10);
        vec.add((byte) 0b111011);
        vec.add((byte) 0b10100);
        vec.add((byte) 0b11101);
        vec.add((byte) 0b1);
        vec.add((byte) 0b1100011);
        vec.add((byte) 0b1);
        vec.add((byte) 0b10101);
        vec.add((byte) 0b1101110);
        vec.add((byte) 0b0);
        vec.add((byte) -0b1111011);
        vec.add((byte) 0b10000);
        vec.add((byte) 0b1100010);
        vec.add((byte) -0b1110101);
        vec.add((byte) 0b0);
        vec.add((byte) -0b1010100);
        vec.add((byte) 0b111);
        vec.add((byte) 0b1);
        vec.add((byte) 0b1111);
        vec.add((byte) 0b0);
        vec.add((byte) 0b111);
        vec.add((byte) 0b1);
        vec.add((byte) 0b10001);
        vec.add((byte) 0b100);
        vec.add((byte) 0b1000000);
        vec.add((byte) 0b10);
        vec.add((byte) -0b1111111);
        vec.add((byte) 0b1101);
        vec.add((byte) 0b1);
        vec.add((byte) 0b1000);
        vec.add((byte) 0b10000);
        vec.add((byte) 0b1010010);
        vec.add((byte) 0b1);
        vec.add((byte) 0b111001);
        vec.add((byte) 0b1001101);
        vec.add((byte) 0b1101100);
        vec.add((byte) 0b0);
        vec.add((byte) 0b100100);
        vec.add((byte) 0b10011);
        vec.add((byte) 0b100110);
        vec.add((byte) 0b100111);
        vec.add((byte) 0b1000001);
        vec.add((byte) 0b10);
        vec.add((byte) 0b1100110);
        vec.add((byte) 0b111110);
        vec.add((byte) 0b101100);
        vec.add((byte) 0b1);
        vec.add((byte) 0b111100);
        vec.add((byte) 0b10000);
        vec.add((byte) -0b111001);
        vec.add((byte) 0b1);
        vec.add((byte) -0b1011000);
        vec.add((byte) 0b1101001);
        vec.add((byte) 0b11101);
        vec.add((byte) 0b1000001);
        vec.add((byte) 0b1);
        vec.add((byte) -0b1010);
        vec.add((byte) -0b1011000);
        vec.add((byte) 0b101011);
        vec.add((byte) 0b1);
        vec.add((byte) 0b1100101);
        vec.add((byte) 0b10101);
        vec.add((byte) 0b11100);
        vec.add((byte) 0b10);
        vec.add((byte) 0b0);
        vec.add((byte) 0b1);
        vec.add((byte) 0b1);
        vec.add((byte) 0b1);
        vec.add((byte) -0b1100001);
        vec.add((byte) 0b10100);
        vec.add((byte) 0b100);
        vec.add((byte) 0b101011);
        vec.add((byte) 0b11);
        vec.add((byte) 0b1);
        vec.add((byte) -0b11110);

        Byte[] bytes = vec.toArray(new Byte[vec.size()]);
        byte[] b = new byte[vec.size()];

        for (int i = 0, bytesLength = bytes.length; i < bytesLength; i++) {
            Byte aByte = bytes[i];
            b[i] = aByte;
        }


        Vector<Integer> actual = GroupVarintCompression.decompress(b);

        assertEquals("Wrong compression", expected, actual);

    }

    @Test
    public void testCompress() throws Exception {

    }

    @Test
    public void testCompress3() throws Exception {

    }
}