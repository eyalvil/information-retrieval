package tests;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by eyal on 13/03/2016.
 */
public class IndexReaderReviewTest extends BaseIndexReaderTest {

    @Test
    public void testProductReview() {

        String actual = indexReader.getProductId(545454);
        String exp = null;

        assertEquals(actual, exp);

        actual = indexReader.getProductId(-1);
        assertEquals(actual, exp);

        actual = indexReader.getProductId(4);
        exp = "B000UA0QIQ";
        assertEquals(actual, exp);

        actual = indexReader.getProductId(100);
        exp = "B0019CW0HE";
        assertEquals(actual, exp);

    }

    @Test
    public void testGetReviewScore() throws Exception {

        int actual = indexReader.getReviewScore(4);
        int expected = 2;

        assertEquals(expected, actual);

        actual = indexReader.getReviewScore(5454545);
        expected = -1;
        assertEquals(expected, actual);
    }

    @Test
    public void testGetReviewHelpfulnessNumerator() throws Exception {


        int actual = indexReader.getReviewHelpfulnessNumerator(4);
        int expected = 3;

        assertEquals(expected, actual);

        actual = indexReader.getReviewHelpfulnessNumerator(5454545);
        expected = -1;
        assertEquals(expected, actual);

    }

    @Test
    public void testGetReviewHelpfulnessDenominator() throws Exception {

        int actual = indexReader.getReviewHelpfulnessDenominator(4);
        int expected = 3;

        assertEquals(expected, actual);

        actual = indexReader.getReviewHelpfulnessDenominator(5454545);
        expected = -1;
        assertEquals(expected, actual);
    }

    @Test
    public void testGetReviewLength() throws Exception {

        int actual = indexReader.getReviewLength(4);
        int expected = 41;
        assertEquals(expected, actual);

        actual = indexReader.getReviewLength(40545454);
        expected = -1;
        assertEquals(actual, expected);

    }

    @Test
    public void testNumberOfReviews() throws Exception {


        int expected = 100;
        int actual = indexReader.getNumberOfReviews();

        assertEquals(expected, actual);

    }

    @Override
    protected String getInputFile() {
        return "data/100.txt";
    }


}