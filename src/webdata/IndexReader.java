package webdata;

import webdata.product.ProductIndex;
import webdata.product.ProductIndexReader;
import webdata.review.ReviewIndex;
import webdata.review.ReviewIndexReader;
import webdata.token.TokenIndex;
import webdata.token.TokenIndexReader;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Created by danny on 3/11/16.
 */
public class IndexReader {

    private final ProductIndex productIndex;
    private final TokenIndex tokenIndex;
    private final ReviewIndex reviewIndex;

    public IndexReader(String dir) {
        ReviewIndex reviewIndex1;
        TokenIndex tokenIndex1;
        ProductIndex productIndex1;

        try {

            productIndex1 = new ProductIndexReader().read(dir);
            tokenIndex1 = new TokenIndexReader().read(dir);
            reviewIndex1 = new ReviewIndexReader().read(dir);

        } catch (IOException e) {

            productIndex1 = null;
            tokenIndex1 = null;
            reviewIndex1 = null;

        }

        productIndex = productIndex1;
        tokenIndex = tokenIndex1;
        reviewIndex = reviewIndex1;
    }

    /**
     * Return the ids of the reviews for a given product identifier
     * Note that the integers returned should be sorted by id
     * <p>
     * Returns an empty Enumeration if there are no reviews for this product
     */
    public Enumeration<Integer> getProductReviews(String productId) {

        try {
            return productIndex.getProductReviews(productId);
        } catch (IOException e) {

            System.err.println(String.format("Error: %s", e));

            return new Vector<Integer>().elements();
        }

    }

    /**
     * Return the number of times that a given token (i.e., word) appears in
     * the reviews indexed
     * Returns 0 if there are no reviews containing this token
     */
    public int getTokenCollectionFrequency(String token) {


        return tokenIndex.getTokenCollectionFrequency(token);

    }

    /**
     * Return the number of reviews containing a given token (i.e., word)
     * Returns 0 if there are no reviews containing this token
     */
    public int getTokenFrequency(String token) {

        return tokenIndex.getTokenFrequency(token);

    }

    public Enumeration<Integer> getReviewsWithToken(String token) {

        try {
            return tokenIndex.getReviewsWithToken(token);
        } catch (IOException e) {

            e.printStackTrace();
            return new Vector<Integer>().elements();
        }
    }

    public String getProductId(int reviewId) {

        try {
            return reviewIndex.getProductId(reviewId);
        } catch (IOException e) {
            return "";
        }
    }

    /**
     * Returns the score for a given review
     * Returns -1 if there is no review with the given identifier
     */
    public int getReviewScore(int reviewId) {

        try {
            return reviewIndex.getReviewScore(reviewId);
        } catch (IOException e) {
            return -1;
        }
    }

    /**
     * Returns the numerator for the helpfulness of a given review
     * Returns -1 if there is no review with the given identifier
     */
    public int getReviewHelpfulnessNumerator(int reviewId) {

        try {
            return reviewIndex.getReviewHelpfulnessNumerator(reviewId);
        } catch (IOException e) {
            return -1;
        }
    }

    /**
     * Returns the denominator for the helpfulness of a given review
     * Returns -1 if there is no review with the given identifier
     */
    public int getReviewHelpfulnessDenominator(int reviewId) {

        try {
            return reviewIndex.getReviewHelpfulnessDenominator(reviewId);
        } catch (IOException e) {
            return -1;
        }
    }

    /**
     * Return the number of product reviews available in the system
     */
    public int getNumberOfReviews() {


        try {
            return reviewIndex.size();
        } catch (IOException e) {
            return -1;
        }

    }

    /**
     * Return the number of number of tokens in the system
     * (Tokens should be counted as many times as they appear)
     */
    public int getTokenSizeOfReviews() {

        return tokenIndex.getTokenFrequency();

    }

    /**
     * Returns the number of tokens in a given review
     * Returns -1 if there is no review with the given identifier
     */
    public int getReviewLength(int reviewId) {

        try {
            return reviewIndex.getReviewLength(reviewId);
        } catch (IOException e) {
            return -1;
        }
    }


}
