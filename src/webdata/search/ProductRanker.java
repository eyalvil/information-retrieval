package webdata.search;

import webdata.IndexReader;

import java.util.*;

/**
 * Created by eyal on 22/05/2016.
 */
public class ProductRanker extends AbstractRanker<String> {
    private AbstractRanker<Integer> rankReviewsVec;

    public ProductRanker(IndexReader indexReader, AbstractRanker<Integer> rankReviewsVec) {
        super(indexReader);
        this.rankReviewsVec = rankReviewsVec;
    }

    @Override
    protected List<ItemScore<String>> rank(int totalTokens, Collection<QueryTerm> terms) {

        List<ItemScore<Integer>> itemScores = rankReviewsVec.rank(totalTokens, terms);
        HashMap<String, PairScoreTimes> productGrades = new HashMap<>();

        for (ItemScore<Integer> item :
                itemScores) {

            Integer reviewId = item.getItemId();
            String productId = indexReader.getProductId(reviewId);

            if (productId == null) {
                throw new IllegalStateException();
            }

            PairScoreTimes pairScoreTimes = productGrades.get(productId);
            double finalScore = getFinalScore(reviewId, item.getScore());
            if (pairScoreTimes != null) {

                pairScoreTimes.setScore(pairScoreTimes.getScore() + finalScore);
                pairScoreTimes.setFrequency(pairScoreTimes.getFrequency() + 1);
            } else {

                productGrades.put(productId, new PairScoreTimes(finalScore));
            }

        }

        List<ItemScore<String>> finalGrades = new ArrayList<>();
        for (Map.Entry<String, PairScoreTimes> entry :
                productGrades.entrySet()) {

            Enumeration<Integer> productReviews = indexReader.getProductReviews(entry.getKey());
            int reviewOfProducts = getEnumerationLength(productReviews);

            PairScoreTimes value = entry.getValue();
            ItemScore<String> itemScore = new ItemScore<>(entry.getKey(), value.getScore() / reviewOfProducts);
            finalGrades.add(itemScore);

        }
        return finalGrades;
    }

    private double getFinalScore(Integer reviewId, double queryScore) {

        int helpfulnessNumerator = indexReader.getReviewHelpfulnessNumerator(reviewId);

        int helpfulnessDenominator = indexReader.getReviewHelpfulnessDenominator(reviewId);
        int reviewScore = indexReader.getReviewScore(reviewId);


        double finalScore;
        if (helpfulnessDenominator != 0)
        {
            finalScore = queryScore * (helpfulnessNumerator/helpfulnessDenominator + 1) * reviewScore;
        }
        else
        {
            finalScore = queryScore * reviewScore;
        }
        return finalScore;
    }

    private static int getEnumerationLength(Enumeration<Integer> productReviews) {
        int counterReviewOfProducts = 0;
        while (productReviews.hasMoreElements())
        {
            counterReviewOfProducts ++;
            productReviews.nextElement();
        }
        return counterReviewOfProducts;
    }

    private class PairScoreTimes {



        double score;

        public int getFrequency() {
            return frequency;
        }

        public void setFrequency(int frequency) {
            this.frequency = frequency;
        }

        public double getScore() {
            return score;
        }

        public void setScore(double score) {
            this.score = score;
        }

        int frequency;

        public PairScoreTimes(double score) {
            this.score = score;
            this.frequency = 1;
        }
    }
}
