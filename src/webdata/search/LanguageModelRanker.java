package webdata.search;

import webdata.IndexReader;

import java.util.*;

public class LanguageModelRanker extends AbstractRanker<Integer> {

    private final double lambda;

    public LanguageModelRanker(IndexReader indexReader, double lambda) {
        super(indexReader);
        this.lambda = lambda;
    }

    @Override
    protected List<ItemScore<Integer>> rank(int totalTokens, Collection<QueryTerm> terms) {

        Map<Integer, Double> docScoresMap = new HashMap<>();

        Map<Integer, Map<QueryTerm, Integer>> reviewMap = new HashMap<>();

        for (QueryTerm term : terms) {

            Enumeration<Integer> positingList = term.getPositingList();

            while (positingList.hasMoreElements()) {

                int docId = positingList.nextElement();
                int docFrequency = positingList.nextElement();

                Map<QueryTerm, Integer> docInfo = reviewMap.getOrDefault(docId, new HashMap<>());

                docInfo.put(term, docFrequency);

                reviewMap.put(docId, docInfo);
            }

        }

        Set<Map.Entry<Integer, Map<QueryTerm, Integer>>> reviewsSet = reviewMap.entrySet();

        for (Map.Entry<Integer, Map<QueryTerm, Integer>> review : reviewsSet) {

            int docId = review.getKey();
            Map<QueryTerm, Integer> docInfo = review.getValue();

            for (QueryTerm term : terms) {

                int docFrequency = docInfo.getOrDefault(term, 0);
                double docProb = (double) docFrequency / indexReader.getReviewLength(docId);

                double collectionProb = (double) term.getTokenCollectionFrequency() / totalTokens;

                if (collectionProb == 0) {
                    continue;
                }

                double docScore = (lambda * docProb + (1 - lambda) * collectionProb);

                if (term.queryFrequency > 1) {
                    docScore = Math.pow(docScore, term.queryFrequency);
                }

                docScore *= docScoresMap.getOrDefault(docId, 1d);

                docScoresMap.put(docId, docScore);
            }

        }

        List<ItemScore<Integer>> itemScores = new ArrayList<>();

        for (Map.Entry<Integer, Double> entry : docScoresMap.entrySet()) {
            double normalizedValue = entry.getValue();
            itemScores.add(new ItemScore<>(entry.getKey(), normalizedValue));
        }

        return itemScores;
    }
}
