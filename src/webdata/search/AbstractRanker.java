package webdata.search;

import webdata.IndexReader;

import java.util.*;

/**
 * Created by danny on 5/17/16.
 */
public abstract class AbstractRanker<T> {

    final IndexReader indexReader;

    AbstractRanker(IndexReader indexReader) {
        this.indexReader = indexReader;
    }

    public Vector<T> getTopKReviews(Enumeration<String> query, int k) {

        Map<String, QueryTerm> queryTerms = new HashMap<>();

        while (query.hasMoreElements()) {
            String queryWord = query.nextElement().toLowerCase();

            QueryTerm queryTerm = queryTerms.get(queryWord);
            if (queryTerm != null) {
                queryTerm.queryFrequency++;
                continue;
            }

            Enumeration<Integer> productReviews = indexReader.getReviewsWithToken(queryWord);

            int tokenCollectionFrequency = indexReader.getTokenCollectionFrequency(queryWord);

            int tokenFrequency = indexReader.getTokenFrequency(queryWord);

            QueryTerm term = new QueryTerm(queryWord, productReviews, tokenCollectionFrequency, tokenFrequency);
            queryTerms.put(queryWord, term);

        }

        int totalTokens = indexReader.getTokenSizeOfReviews();
        List<ItemScore<T>> itemScores = rank(totalTokens, queryTerms.values());

        Collections.sort(itemScores, Collections.reverseOrder());

        Vector<T> returnValue = new Vector<>();

        for (int i = 0; i < Math.min(k, itemScores.size()); i++) {
            ItemScore<T> itemScore = itemScores.get(i);
            T docId = itemScore.itemId;
            returnValue.add(docId);
        }

        return returnValue;

    }

    protected abstract List<ItemScore<T>> rank(int totalTokens, Collection<QueryTerm> terms);

    static class QueryTerm {

        private String term;

        private Enumeration<Integer> positingList;

        private int tokenCollectionFrequency;

        private int tokenFrequency;

        int queryFrequency;

        QueryTerm(String term, Enumeration<Integer> positingList, int TokenCollectionFrequency, int tokenFrequency) {
            this.term = term;
            this.positingList = positingList;
            this.tokenCollectionFrequency = TokenCollectionFrequency;
            this.tokenFrequency = tokenFrequency;
            this.queryFrequency = 1;
        }

        public String getTerm() {
            return term;
        }

        Enumeration<Integer> getPositingList() {
            return positingList;
        }

        public int getTokenCollectionFrequency() {
            return tokenCollectionFrequency;
        }

        int getTokenFrequency() {
            return tokenFrequency;
        }

        int getQueryFrequency() {
            return queryFrequency;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            QueryTerm queryTerm = (QueryTerm) o;

            return term != null ? term.equals(queryTerm.term) : queryTerm.term == null;

        }

        @Override
        public int hashCode() {
            return term != null ? term.hashCode() : 0;
        }

        @Override
        public String toString() {
            return "QueryTerm{" +
                    "positingList=" + positingList +
                    ", term='" + term + '\'' +
                    ", tokenCollectionFrequency=" + tokenCollectionFrequency +
                    ", tokenFrequency=" + tokenFrequency +
                    ", queryFrequency=" + queryFrequency +
                    '}';
        }
    }

    static class ItemScore<T> implements Comparable<ItemScore> {

        public T getItemId() {
            return itemId;
        }

        public double getScore() {
            return score;
        }

        private T itemId;

        private double score;

        ItemScore(T itemId, double score) {
            this.itemId = itemId;
            this.score = score;
        }

        @Override
        public int compareTo(ItemScore o) {

            if (o == null) {
                return 1;
            }

            return Double.compare(score, o.score);
        }

        @Override
        public String toString() {
            return "ItemScore{" +
                    "itemId=" + itemId +
                    ", score=" + score +
                    '}';
        }
    }
}
