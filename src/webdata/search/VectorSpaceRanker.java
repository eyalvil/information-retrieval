package webdata.search;

import webdata.IndexReader;

import java.util.*;

/**
 * Created by danny on 5/17/16.
 */
public class VectorSpaceRanker extends AbstractRanker<Integer> {

    public VectorSpaceRanker(IndexReader indexReader) {
        super(indexReader);
    }

    protected List<ItemScore<Integer>> rank(int totalTokens, Collection<QueryTerm> queryTermList) {

        Map<Integer, Double> docScoresMap = new HashMap<>();

        List<Double> weights = new ArrayList<>();

        for (QueryTerm queryTerm : queryTermList) {

            int tokenFrequency = queryTerm.getTokenFrequency();

            if (tokenFrequency == 0) {
                continue;
            }

            int totalReviews = indexReader.getNumberOfReviews();
            double queryWeight = logTermFrequency(queryTerm.getQueryFrequency()) * documentInverseFrequency(tokenFrequency, totalReviews);


            Enumeration<Integer> positingList = queryTerm.getPositingList();

            while (positingList.hasMoreElements()) {

                int docId = positingList.nextElement();

                int docFrequency = positingList.nextElement();

                double docWeight = logTermFrequency(docFrequency);

                double docScore = docScoresMap.getOrDefault(docId, 0d) + docWeight * queryWeight;

                docScoresMap.put(docId, docScore);

            }

            weights.add(queryWeight);

        }

        double normalizationFactor = cosineNormalization(weights);

        List<ItemScore<Integer>> itemScores = new ArrayList<>();

        for (Map.Entry<Integer, Double> entry : docScoresMap.entrySet()) {
            double normalizedValue = entry.getValue() / normalizationFactor;
            itemScores.add(new ItemScore<>(entry.getKey(), normalizedValue));
        }

        return itemScores;
    }

    private double cosineNormalization(List<Double> weights) {

        double sum = 0;
        for (Double weight : weights) {
            sum += weight * weight;
        }
        sum = Math.sqrt(sum);
        return sum;
    }

    private double logTermFrequency(int queryFrequency) {
        return 1 + Math.log(queryFrequency);
    }

    private double documentInverseFrequency(int tokenFrequency, int totalTokens) {
        return Math.log(totalTokens / tokenFrequency);
    }
}
