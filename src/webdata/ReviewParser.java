package webdata;

import webdata.review.Review;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by danny on 3/11/16.
 */
class ReviewParser implements Closeable {


    private static final String PRODUCT_PREFIX = "product/productId: ";

    private static final int PRODUCT_PREFIX_LENGTH = PRODUCT_PREFIX.length();

    private static final String SCORE_PREFIX = "review/score: ";

    private static final int SCORE_PREFIX_LENGTH = SCORE_PREFIX.length();

    private static final String TEXT_PREFIX = "review/text: ";

    private static final int TEXT_PREFIX_LENGTH = TEXT_PREFIX.length();

    private static final String regex = "[^A-Za-z0-9 ]";

    private static Pattern helpfulnessPattern = Pattern.compile("review/helpfulness: (\\d+)/(\\d+)");

    private static Pattern alphaNumericPattern = Pattern.compile("review/helpfulness: (\\d+)/(\\d+)");

    private final BufferedReader bufferedReader;
    private int i;

    ReviewParser(String file) throws FileNotFoundException {

        bufferedReader = new BufferedReader(new FileReader(file));

    }

    Review nextReview() throws IOException {

        i++;

        Review.ReviewBuilder builder = Review.builder();

        String line = bufferedReader.readLine();

        if (line == null || line.isEmpty()) {
            return null;
        }

        String productId = line.substring(PRODUCT_PREFIX_LENGTH);
        builder.setProductId(productId);

        bufferedReader.readLine(); // ignore userId
        bufferedReader.readLine(); // ignore profileName

        String helpfulness = bufferedReader.readLine();
        while (!helpfulness.startsWith("review/helpfulness")) {
            helpfulness = bufferedReader.readLine();
        }

        Matcher matcher = helpfulnessPattern.matcher(helpfulness);

        if (matcher.find()) {
            builder.setHelpfulnessNumerator(Integer.parseInt(matcher.group(1)));
            builder.setHelpfulnessDenominator(Integer.parseInt(matcher.group(2)));
        } else {
            throw new IllegalStateException(String.format("Incorrect Helpfulness Pattern: %s Review id: %s", helpfulness, productId));
        }

        String scoreString = bufferedReader.readLine().substring(SCORE_PREFIX_LENGTH);
        builder.setScore(Short.parseShort(String.valueOf(scoreString.charAt(0))));

        bufferedReader.readLine(); // ignore time
        bufferedReader.readLine(); // ignore summary

        String rawText = bufferedReader.readLine().substring(TEXT_PREFIX_LENGTH);


        // read until empty line
        String addiontalText = bufferedReader.readLine();
        while (!addiontalText.isEmpty()) {
        	
        	rawText += addiontalText;
        	addiontalText = bufferedReader.readLine();

        }

        StringBuilder stringBuilder = new StringBuilder(rawText.length());

        for (int i = 0; i < rawText.length(); i++) {
            char ch = rawText.charAt(i);
            if (Character.isLetterOrDigit(ch) || ch == ' ') {
                stringBuilder.append(ch);
            } else {
                stringBuilder.append(' ');
            }
        }

        String text = stringBuilder.toString();

        StringTokenizer stringTokenizer = new StringTokenizer(text, " ");

        List<String> tokenList = new ArrayList<>();

        builder.setTokenCount(stringTokenizer.countTokens());

        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken().toLowerCase();
            tokenList.add(token);

        }

        builder.setTokens(tokenList);

        return builder.createReview();
    }

    public void close() throws IOException {

        bufferedReader.close();

    }

}
