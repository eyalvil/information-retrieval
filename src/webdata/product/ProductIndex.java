package webdata.product;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Created by danny on 3/11/16.
 */
public class ProductIndex {

    private final ProductDictionary productDictionary;
    private final ProductInvertedIndex.Reader reader;

    public ProductIndex(ProductDictionary productDictionary, ProductInvertedIndex.Reader reader) {

        this.productDictionary = productDictionary;
        this.reader = reader;
    }

    public Enumeration<Integer> getProductReviews(String productId) throws IOException {

        ProductDictionary.ProductMetaData metaData = productDictionary.getMetaData(productId);



        if (metaData == null) {
            return new Vector<Integer>().elements();
        }

        return reader.retrieve(metaData.positingPtr).reviewsId.elements();
    }
}
