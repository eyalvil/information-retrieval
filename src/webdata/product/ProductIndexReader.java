package webdata.product;

import java.io.File;
import java.io.IOException;

/**
 * Created by danny on 3/11/16.
 */
public class ProductIndexReader {

    public ProductIndexReader() {

    }

    public ProductIndex read(String dir) throws IOException {

        ProductDictionary.Reader reader = new ProductDictionary.Reader(dir + File.separatorChar +  "product.dict");
        ProductDictionary productDictionary = reader.read();

        ProductInvertedIndex.Reader reader1 = new ProductInvertedIndex.Reader(dir + File.separatorChar + "product.index");

        return new ProductIndex(productDictionary, reader1);
    }

}
