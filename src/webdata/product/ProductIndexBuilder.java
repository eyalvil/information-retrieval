package webdata.product;


import webdata.utils.Pair;

import java.io.File;
import java.io.IOException;

/**
 * Created by danny on 3/11/16.
 */
public class ProductIndexBuilder {

    private ProductInvertedIndex.InvertedIndexBuilder indexBuilder = new ProductInvertedIndex.InvertedIndexBuilder();

    private ProductDictionary.Builder productDictionaryBuilder = new ProductDictionary.Builder();

    public void addProduct(String product, int reviewId) {

        indexBuilder.addAppearance(product, reviewId);

    }


    public void write(String path) throws IOException {

        ProductInvertedIndex invertedIndex = indexBuilder.build();

        ProductInvertedIndex.Writer invertedIndexWriter = new ProductInvertedIndex.Writer(path + File.separatorChar + "product.index", invertedIndex);

        String product = invertedIndexWriter.nextProduct();

        while (product != null) {

            Pair<Integer, Long> ptr = invertedIndexWriter.writeNextProduct();

            productDictionaryBuilder.addTerm(product, ptr.getRight());

            product = invertedIndexWriter.nextProduct();
        }

        invertedIndexWriter.close();

        ProductDictionary productDictionary = productDictionaryBuilder.build();

        ProductDictionary.Writer productWriter = new ProductDictionary.Writer(path + File.separatorChar + "product.dict");
        productWriter.write(productDictionary);
        productWriter.close();

        productDictionaryBuilder.close();
        productDictionaryBuilder = null;
        indexBuilder = null;

    }
}
