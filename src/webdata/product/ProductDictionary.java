package webdata.product;

import java.io.*;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by danny on 3/1/16.
 */
public class ProductDictionary {

    private Map<String, ProductMetaData> map;

    public ProductDictionary(Map<String, ProductMetaData> map) {

        this.map = map;
    }

    public ProductMetaData getMetaData(String product) {


        return map.get(product);
    }

    static class ProductMetaData {


        long positingPtr;


        public ProductMetaData(long positingPtr) {
            this.positingPtr = positingPtr;
        }
    }

    static class Writer {

        private final DataOutputStream file;

        public Writer(String file) throws FileNotFoundException {

            this.file = new DataOutputStream(new FileOutputStream(file));

        }

        public void write(ProductDictionary productDictionary) throws IOException {

            Set<Map.Entry<String, ProductMetaData>> entries = productDictionary.map.entrySet();

            file.writeInt(entries.size());

            for (Map.Entry<String, ProductMetaData> entry : entries) {

                String key = entry.getKey();

                char[] charArray = key.toCharArray();
                for (int i = 0; i < 10; i++) {
                    char c = charArray[i];
                    file.writeChar(c);
                }


                ProductMetaData value = entry.getValue();
                file.writeLong(value.positingPtr);

            }

        }

        public void close() throws IOException {

            file.close();
        }

    }

    static class Reader {

        private final DataInputStream file;

        public Reader(String file) throws FileNotFoundException {

            this.file = new DataInputStream(new FileInputStream(file));
        }

        public ProductDictionary read() throws IOException {


            int items = file.readInt();

            Builder builder = new Builder();

            for (int i = 0; i < items; i++) {

                StringBuilder stringBuilder = new StringBuilder(10);

                for (int j = 0; j < 10; j++) {
                    stringBuilder.append(file.readChar());
                }


                long ptr = file.readLong();

                builder.addTerm(stringBuilder.toString(), ptr);
            }

            return builder.build();

        }
    }

    static class Builder {

        private TreeMap<String, ProductMetaData> terms = new TreeMap<>();

        Builder() {
        }

        void addTerm(String product, long postingPtr) {

            terms.put(product, new ProductMetaData(postingPtr));

        }

        ProductDictionary build() {

            return new ProductDictionary(terms);

        }


        public void close() {

            terms.clear();
        }
    }

}
