package webdata.product;

import webdata.utils.GroupVarintCompression;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Vector;

/**
 * Created by danny on 3/11/16.
 */
public class ProductPostingList {

    Vector<Integer> reviewsId;

    public ProductPostingList(Vector<Integer> reviewsId) {

        this.reviewsId = reviewsId;
    }

    public static class Writer {

        private final DataOutput dataoutput;

        public Writer(DataOutput dataoutput) {

            this.dataoutput = dataoutput;
        }

        public void write(ProductPostingList productPostingList) throws IOException {

            Vector<Integer> reviewsId = productPostingList.reviewsId;
            Vector<Byte> compressed = GroupVarintCompression.compress(reviewsId);

            dataoutput.writeInt(compressed.size());

            for (Byte aByte : compressed) {
                dataoutput.writeByte(aByte);
            }


        }

    }

    public static class Reader {

        private DataInput dataInput;

        public Reader(DataInput dataInput) {

            this.dataInput = dataInput;
        }

        public ProductPostingList readProductPostingList() throws IOException {

            int bsize = dataInput.readInt();

            byte[] bytes = new byte[bsize];
            dataInput.readFully(bytes);

            Vector<Integer> list = GroupVarintCompression.decompress(bytes);
            return new ProductPostingList(list);

        }
    }

}
