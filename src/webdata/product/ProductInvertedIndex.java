package webdata.product;

import webdata.utils.Pair;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

/**
 * Created by danny on 3/1/16.
 */
public class ProductInvertedIndex {

    Map<String, ProductPostingList> map;

    public ProductInvertedIndex(Map<String, ProductPostingList> map) {

        this.map = map;
    }

    public static class Writer {

        private final RandomAccessFile rw;

        private final Iterator<Map.Entry<String, ProductPostingList>> invertedIndexIterator;

        private final ProductPostingList.Writer postingListWriter;

        private Map.Entry<String, ProductPostingList> current;

        public Writer(String file, ProductInvertedIndex ProductInvertedIndex) throws FileNotFoundException {
            rw = new RandomAccessFile(file, "rw");
            this.invertedIndexIterator = ProductInvertedIndex.map.entrySet().iterator();
            this.postingListWriter = new ProductPostingList.Writer(rw);
        }

        String nextProduct() {

            if (invertedIndexIterator.hasNext()) {
                current = invertedIndexIterator.next();
                return current.getKey();
            } else {
                return null;
            }

        }

        Pair<Integer, Long> writeNextProduct() throws IOException {

            if (current == null) {
                throw new IllegalStateException("Should call nextProductBefore()");
            }

            long ptr = rw.getFilePointer();

            ProductPostingList value = current.getValue();

            postingListWriter.write(value);

            return new Pair<>(value.reviewsId.size(), ptr);
        }

        public void close() throws IOException {

            rw.close();
        }
    }

    public static class Reader {

        private final RandomAccessFile file;
        private final ProductPostingList.Reader ProductPostingListReader;

        public Reader(String path) throws FileNotFoundException {
            file = new RandomAccessFile(path, "r");
            ProductPostingListReader = new ProductPostingList.Reader(file);
        }

        public ProductPostingList retrieve(long ptr) throws IOException {

            file.seek(ptr);

            return ProductPostingListReader.readProductPostingList();
        }

    }


    public static class InvertedIndexBuilder {

        private Map<String, ProductPostingList> map = new HashMap<>();

        public void addAppearance(String term, int reviewId) {

            ProductPostingList list = map.get(term);

            if (list == null) {

                list = new ProductPostingList(new Vector<>()); // builder
                map.put(term, list);

            }

            list.reviewsId.add(reviewId);

        }

        public ProductInvertedIndex build() {

            return new ProductInvertedIndex(map);
        }

    }


}
