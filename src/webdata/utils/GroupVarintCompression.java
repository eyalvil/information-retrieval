package webdata.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Vector;

public class GroupVarintCompression {


    public static Vector<Byte> compress(Vector<Integer> toCompress) {
        return compress(toCompress, true);
    }

    public static Vector<Byte> compress(Vector<Integer> toCompress, boolean gapsStorage) {

        Vector<Byte> bytes = new Vector<>();

        int accumulator = 0;
        for (int i = 0, len = toCompress.size(); i < len; i += 4) {

            int min = Math.min(i + 4, len);
            List<Integer> list = toCompress.subList(i, min);

            int group = 0x0;

            Vector<Byte> groupVec = new Vector<>();

            for (int i1 = 0, listSize = list.size(); i1 < listSize; i1++) {

                int val = list.get(i1);

                if (gapsStorage) {
                    val = val - accumulator;
                    accumulator += val;
                }

                int groupSize = ((int) (Math.log(val) / Math.log(2)) / 8);
                group |= groupSize;

                if (i1 + 1 != listSize) {
                    group = (group << 2);
                }

                int mask = 0xFF000000;
                int i2 = groupSize + 1;
                mask >>>= (8 * (4 - i2));
                for (int j = 0; j < i2; j++) {
                    int b = (mask & val);

                    b >>>= (i2 - 1 - j) * 8;

                    groupVec.add((byte) b);
                    mask >>>= 8;
                }
            }

            for (int j = 0; j < 4 - list.size(); j++) {
                group = (group << 2);
            }

            bytes.add((byte) group);
            bytes.addAll(groupVec);


        }

        return bytes;

    }

    public static Vector<Integer> decompress(byte[] bytes) {
        return decompress(bytes, true);
    }

    public static Vector<Integer> decompress(byte[] bytes, final boolean gapsStorage) {

        Vector<Integer> vec = new Vector<>();

        int i = 0;
        int bytesSize = bytes.length;
        int accumulate = 0;
        while (i < bytesSize) {

            int group = bytes[i++];

            for (int j = 0; j < 4 && i < bytesSize; j++) {
                int groupSize = (group & 0xC0);
                groupSize = groupSize >> 6;
                groupSize += 1;
                group = group << 2;
                int groupBinarySize = i + groupSize;
                byte[] range = Arrays.copyOfRange(bytes, i, groupBinarySize);

                int val = convertByteToInt(range);

                if (gapsStorage) {
                    val += accumulate;
                    accumulate = val;
                }


                i = groupBinarySize;
                vec.add(val);
            }
        }


        return vec;
    }

    public static int convertByteToInt(byte[] b) {

        int res = 0;


        if (b == null) {
            return res;
        }

        for (byte aB : b) {

            res = (res << 8);

            int c = aB & 0xFF;

            res += c;
        }
        return res;
    }

}
