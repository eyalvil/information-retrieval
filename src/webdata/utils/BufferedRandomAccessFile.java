package webdata.utils;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

/**
 * Created by danny on 4/16/16.
 */
public class BufferedRandomAccessFile {

    private final RandomAccessFile randomAccessFile;

    private ByteBuffer writeBuffer;

    private ByteBuffer readBuffer;

    private int written = 0;

    public BufferedRandomAccessFile(RandomAccessFile randomAccessFile) {

        this.randomAccessFile = randomAccessFile;

    }

    public void init(int BLOCK_SIZE) {

        int blockSize = BLOCK_SIZE * 8;

        this.writeBuffer = ByteBuffer.allocate(blockSize);
        writeBuffer.limit(blockSize);

        this.readBuffer = ByteBuffer.allocate(blockSize);
        readBuffer.limit(blockSize);
        readBuffer.position(blockSize);

    }

    public RandomAccessFile getRandomAccessFile() {
        return randomAccessFile;
    }

    public void writeInt(int val) throws IOException {

        written += 4;
        writeBuffer.putInt(val);

        if (writeBuffer.position() >= writeBuffer.limit()) {
            flush();
        }

    }

    public void seek(long ptr) throws IOException {

        randomAccessFile.seek(ptr);
        readBuffer.clear();
        writeBuffer.clear();
        randomAccessFile.read(readBuffer.array());

    }

    public int readInt() throws IOException {

        if (readBuffer.position() >= writeBuffer.limit()) {
            readBuffer.clear();
            randomAccessFile.read(readBuffer.array());
        }

        return readBuffer.getInt();

    }

    public void flush() throws IOException {

        randomAccessFile.write(writeBuffer.array(), 0, written);
        writeBuffer.clear();
        written = 0;
    }

    public void setLength(int i) throws IOException {

        readBuffer.clear();
        writeBuffer.clear();
        randomAccessFile.setLength(0);


    }

    public long length() throws IOException {
        return randomAccessFile.length();
    }

    public long getFilePointerBlockStart() throws IOException {
        return randomAccessFile.getFilePointer();
    }

    public void close() {

        writeBuffer.clear();
        writeBuffer = null;
    }
}
