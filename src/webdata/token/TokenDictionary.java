package webdata.token;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by danny on 3/1/16.
 */
class TokenDictionary {

    private Map<String, TokenMetaData> map;

    private int tokenFrequency;

    private TokenDictionary(Map<String, TokenMetaData> map, int tokenFrequency) {

        this.map = map;
        this.tokenFrequency = tokenFrequency;
    }

    TokenMetaData getMetaData(String token) {

        return map.get(token);
    }

    int tokenFrequency() {

        return tokenFrequency;
    }

    static class TokenMetaData {

        private int frequency;

        private int count;

        private long positingPtr;


        int getFrequency() {
            return frequency;
        }

        int getCount() {
            return count;
        }

        long getPositingPtr() {
            return positingPtr;
        }

        TokenMetaData(int frequency, int count, long positingPtr) {
            this.frequency = frequency;
            this.count = count;
            this.positingPtr = positingPtr;
        }
    }

    private static class TokenMetaDataBuilder {

        private int frequency = 0;

        private int count = 0;

        private long positingPtr = 0;

        private int lastReviewId = -1;

        void addAppearance(int reviewId) {

            this.count++;

            if (lastReviewId != reviewId) {
                frequency++;
                lastReviewId = reviewId;
            }

        }

        TokenMetaDataBuilder setPositingPtr(long positingPtr) {
            this.positingPtr = positingPtr;
            return this;
        }

        TokenMetaData build() {
            return new TokenMetaData(frequency, count, positingPtr);
        }
    }

    static class Builder {

        private TreeMap<String, TokenDictionary.TokenMetaDataBuilder> terms = new TreeMap<>();

        private HashMap<String, Integer> termsId = new HashMap<>();

        private HashMap<Integer, String> termsIdInverse = new HashMap<>();

        private int counter = 1;

        private int tokenFrequency;

        Builder() {
        }

        int addTerm(String term, int reviewId) {

            TokenDictionary.TokenMetaDataBuilder tokenInfo = terms.get(term);
            if (tokenInfo == null) {
                tokenInfo = new TokenMetaDataBuilder();
                terms.put(term, tokenInfo);
                int termId = counter++;
                termsId.put(term, termId);
                termsIdInverse.put(termId, term);

            }

            tokenInfo.addAppearance(reviewId);

            tokenFrequency++;

            return termsId.get(term);
        }

        void addPostingPtr(int term, long ptr) {

            String token = termsIdInverse.get(term);

            if (token == null) {
                System.out.println(term);
                throw new IllegalStateException("Null token");
            }

            terms.get(token).setPositingPtr(ptr);

        }

        TokenDictionary build() {

            TreeMap<String, TokenMetaData> tokens = new TreeMap<>();

            for (Map.Entry<String, TokenMetaDataBuilder> entry : terms.entrySet()) {
                tokens.put(entry.getKey(), entry.getValue().build());
            }

            return new TokenDictionary(tokens, tokenFrequency);
        }
    }

    public static class Writer {

        private final DataOutputStream file;

        public Writer(String file) throws FileNotFoundException {

            this.file = new DataOutputStream(new FileOutputStream(file));

        }

        public void write(TokenDictionary tokenDictionary) throws IOException {

            file.writeInt(tokenDictionary.tokenFrequency);

            Set<Map.Entry<String, TokenMetaData>> entries = tokenDictionary.map.entrySet();

            file.writeInt(entries.size());

            for (Map.Entry<String, TokenMetaData> entry : entries) {

                String key = entry.getKey();

                file.writeUTF(key);

                TokenMetaData meta = entry.getValue();
                file.writeInt(meta.frequency);
                file.writeInt(meta.count);
                file.writeLong(meta.positingPtr);
            }


        }

        public void close() throws IOException {

            file.close();
        }
    }

    public static class Reader {

        private final DataInputStream file;

        public Reader(String file) throws FileNotFoundException {

            this.file = new DataInputStream(new FileInputStream(file));
        }

        public TokenDictionary read() throws IOException {

            int tokenFrequency = file.readInt();
            int items = file.readInt();

            Map<String, TokenMetaData> map = new HashMap<>();

            for (int i = 0; i < items; i++) {


                String key = file.readUTF();

                int frequency = file.readInt();
                int count = file.readInt();
                long positingPtr = file.readLong();
                TokenMetaData tokenMetaData = new TokenMetaData(frequency, count, positingPtr);

                map.put(key, tokenMetaData);
            }

            return new TokenDictionary(map, tokenFrequency);

        }
    }


}
