package webdata.token;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by danny on 3/11/16.
 */
public class TokenIndexBuilder {

    private TokenInvertedIndex.InvertedIndexWriterBuilder indexBuilder;

    private TokenDictionary.Builder dictionaryBuilder = new TokenDictionary.Builder();
    private String dir;

    public TokenIndexBuilder(String dir) throws FileNotFoundException {
        this.dir = dir;
        indexBuilder = new TokenInvertedIndex.InvertedIndexWriterBuilder(dir);
    }

    public void addTerm(String term, int reviewId) throws IOException {

        int termId = dictionaryBuilder.addTerm(term, reviewId);
        indexBuilder.addAppearance(termId, reviewId);

    }


    public void write() throws IOException {

        TokenInvertedIndex.Writer invertedIndexWriter = indexBuilder.build(dir);

        int tokenId = invertedIndexWriter.nextToken();


        while (tokenId != -1) {

            long ptr = invertedIndexWriter.writeNextToken();

            dictionaryBuilder.addPostingPtr(tokenId, ptr);

            tokenId = invertedIndexWriter.nextToken();

        }

        TokenDictionary tokenDictionary = dictionaryBuilder.build();

        TokenDictionary.Writer tokenDictionaryWriter = new TokenDictionary.Writer(dir + File.separatorChar + TokenIndex.DICTIONARY_FILE);
        tokenDictionaryWriter.write(tokenDictionary);

        invertedIndexWriter.close();
        tokenDictionaryWriter.close();
        indexBuilder.close();

    }
}
