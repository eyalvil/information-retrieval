package webdata.token;

import webdata.ExternalSort;
import webdata.utils.BufferedRandomAccessFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by danny on 3/1/16.
 */
class TokenInvertedIndex {

    public static class Writer {

        private final RandomAccessFile rw;

        private final TokenPostingList.Writer postingListWriter;

        private final RandomAccessFile sortedPairsFile;

        private final BufferedRandomAccessFile bufferedSortedPairs;

        private int current = -2;

        public Writer(String file, RandomAccessFile tokenInvertedIndex) throws IOException {
            rw = new RandomAccessFile(file, "rw");

            this.sortedPairsFile = tokenInvertedIndex;
            sortedPairsFile.seek(sortedPairsFile.length());
            sortedPairsFile.writeInt(-1);
            sortedPairsFile.seek(0);

            this.bufferedSortedPairs = new BufferedRandomAccessFile(sortedPairsFile);
            bufferedSortedPairs.init(10000000);

            this.postingListWriter = new TokenPostingList.Writer(rw);
        }

        int nextToken() throws IOException {

            if (current == -2) {
                current = bufferedSortedPairs.readInt();
            }

            return current;

        }

        long writeNextToken() throws IOException {

            long ptr = rw.getFilePointer();

            if (current == -1) {
                throw new IllegalStateException("Should call nextTokenBefore()");
            }

            List<TokenPostingList.MetaData> list = new ArrayList<>();

            int termId = current;
            int lastReviewId = -1;

            if (termId == 6310) {
                System.out.println();
            }

            TokenPostingList.MetaData metaData = null;

            while (termId == current) {

                int reviewId = bufferedSortedPairs.readInt();

                if (reviewId != lastReviewId) {
                    metaData = new TokenPostingList.MetaData(reviewId, 1);
                    list.add(metaData);
                    lastReviewId = reviewId;
                } else {
                    if (metaData == null) {
                        throw new AssertionError(String.format("Termid %d , Review id %s lastReviewId %s", termId, reviewId, lastReviewId));
                    }
                    metaData.addAppearance();
                }

                termId = bufferedSortedPairs.readInt();
            }

            current = termId;

            long add = postingListWriter.write(new TokenPostingList(list));

            return ptr + add;
        }

        public void close() throws IOException {

            postingListWriter.close();
            rw.close();

        }

    }

    public static class Reader {

        private final RandomAccessFile file;
        private final TokenPostingList.Reader tokenPostingListReader;

        public Reader(String path) throws FileNotFoundException {
            file = new RandomAccessFile(path, "r");
            tokenPostingListReader = new TokenPostingList.Reader(file);
        }

        TokenPostingList retrieve(long ptr, int size) throws IOException {

            file.seek(ptr);

            return tokenPostingListReader.readPostingList(size);
        }

    }


    static class InvertedIndexWriterBuilder {

        private final File pairsFile;

        private final BufferedRandomAccessFile pairsFileRAF;

        private ExternalSort externalSort;

        InvertedIndexWriterBuilder(String dir) throws FileNotFoundException {

            pairsFile = new File(dir + File.separatorChar + "pairs.tmp");
            pairsFileRAF = new BufferedRandomAccessFile(new RandomAccessFile(pairsFile, "rw"));
            pairsFileRAF.init(10000000);
        }

        void addAppearance(int termId, int reviewId) throws IOException {

            if (termId < 0) {
                throw new IllegalStateException("Illegal term id: " + termId);
            }

            pairsFileRAF.writeInt(termId);
            pairsFileRAF.writeInt(reviewId);

        }

        Writer build(String dir) throws IOException {

            pairsFileRAF.flush();

            RandomAccessFile randomAccessFile = pairsFileRAF.getRandomAccessFile();
            externalSort = new ExternalSort(randomAccessFile, dir);
            RandomAccessFile outputFile = externalSort.sort();

            externalSort.close();

            return new Writer(dir + File.separatorChar + TokenIndex.INDEX_FILE, outputFile);

        }

        void close() {

            pairsFile.delete();
            externalSort.close();

        }
    }


}
