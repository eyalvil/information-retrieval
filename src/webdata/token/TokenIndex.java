package webdata.token;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Created by danny on 3/11/16.
 */
public class TokenIndex {

    public static final String INDEX_FILE = "token.index";

    public static final String DICTIONARY_FILE = "token.dict";

    private final TokenDictionary tokenDictionary;

    private final TokenInvertedIndex.Reader tokenIndexReader;

    public TokenIndex(TokenDictionary tokenDictionary, TokenInvertedIndex.Reader tokenIndexReader) {

        this.tokenDictionary = tokenDictionary;
        this.tokenIndexReader = tokenIndexReader;
    }


    public int getTokenCollectionFrequency(String token) {

        TokenDictionary.TokenMetaData metaData = getTokenMetaData(token);

        if (metaData == null) {
            return 0;
        }

        return metaData.getCount();
    }

    private TokenDictionary.TokenMetaData getTokenMetaData(String token) {
        return tokenDictionary.getMetaData(token.toLowerCase());
    }

    public int getTokenFrequency(String token) {

        TokenDictionary.TokenMetaData tokenMetaData = getTokenMetaData(token);

        if (tokenMetaData == null) {
            return 0;
        }

        return tokenMetaData.getFrequency();
    }

    public Enumeration<Integer> getReviewsWithToken(String token) throws IOException {

        TokenDictionary.TokenMetaData tokenMetaData = getTokenMetaData(token);
        if (tokenMetaData == null) {
            return new Vector<Integer>().elements();
        }

        long ptr = tokenMetaData.getPositingPtr();
        int length = tokenMetaData.getFrequency();

        TokenPostingList postingList = tokenIndexReader.retrieve(ptr, length);

        return postingList.enumeration();
    }

    public int getTokenFrequency() {

        return tokenDictionary.tokenFrequency();
    }

}
