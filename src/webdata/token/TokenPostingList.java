package webdata.token;

import webdata.utils.GroupVarintCompression;

import java.io.DataInput;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * Created by danny on 3/11/16.
 */
class TokenPostingList /*implements Enumeration<Integer>*/ {

    private List<MetaData> reviews; // todo sort

    TokenPostingList(List<MetaData> reviews) {
        this.reviews = reviews;
    }

    public MetaData getMetaData(int reviewId) {

        for (MetaData review : reviews) {
            if (review.reviewId == reviewId) {
                return review;
            }
        }

        return null;

    }

    Enumeration<Integer> enumeration() {
        Vector<Integer> vec = new Vector<>();
        for (MetaData review : reviews) {
            vec.add(review.reviewId);

            if (review.reviewId > 10000) {
                throw new IllegalStateException();
            }

            vec.add(review.frequency);
        }
        return vec.elements();
    }

    public static class Reader {

        private DataInput dataInput;

        public Reader(DataInput dataInput) {

            this.dataInput = dataInput;
        }

        TokenPostingList readPostingList(int size) throws IOException {

            List<MetaData> list = new ArrayList<>();

            byte[] bytes = readBytesArray();
            Vector<Integer> vec1 = GroupVarintCompression.decompress(bytes);

            bytes = readBytesArray();
            Vector<Integer> vec2 = GroupVarintCompression.decompress(bytes, false);

            for (int i = 0; i < size; i++) {

                int reviewId = vec1.get(i);
                int frequency = vec2.get(i);

                MetaData metaData = new MetaData(reviewId, frequency);
                list.add(metaData);
            }

            return new TokenPostingList(list);

        }

        byte[] readBytesArray() throws IOException {

            int bsize = dataInput.readInt();

            byte[] bytes = new byte[bsize];
            dataInput.readFully(bytes);
            return bytes;
        }

    }

    static class Writer {

        private final RandomAccessFile dataoutput;

        private ByteBuffer writeBuffer;

        public Writer(RandomAccessFile dataoutput) {

            this.dataoutput = dataoutput;
            this.writeBuffer = ByteBuffer.allocate(64000000);
        }

        public long write(TokenPostingList tokenPostingList) throws IOException {


            Vector<Integer> reviewIdVec = new Vector<>();
            Vector<Integer> freqVec = new Vector<>();

            for (MetaData review : tokenPostingList.reviews) {
                reviewIdVec.add(review.reviewId);
                freqVec.add(review.frequency);
            }

            Vector<Byte> vec1 = GroupVarintCompression.compress(reviewIdVec);

            Vector<Byte> vec2 = GroupVarintCompression.compress(freqVec, false);

            long ptr = writeByteVec(vec1);
            writeByteVec(vec2);

            return ptr;
        }

        long writeByteVec(Vector<Byte> vec1) throws IOException {

            long position = writeBuffer.position();

            int remaining = writeBuffer.remaining();
            if (remaining < vec1.size() + 4) {
                dataoutput.write(writeBuffer.array());
                writeBuffer.clear();
            }


            writeBuffer.putInt(vec1.size());

            for (Byte aByte : vec1) {
                writeBuffer.put(aByte);
            }

            return position;


        }

        void close() throws IOException {

            byte[] bytes = Arrays.copyOfRange(writeBuffer.array(), writeBuffer.arrayOffset(), writeBuffer.position());
            dataoutput.write(bytes);

        }

    }

    static class MetaData {

        private int reviewId;

        private int frequency;

        MetaData(int reviewId, int frequency) {
            this.reviewId = reviewId;
            this.frequency = frequency;
        }

        void addAppearance() {

            this.frequency++;
        }

    }
}
