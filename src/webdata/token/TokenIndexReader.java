package webdata.token;

import java.io.File;
import java.io.IOException;

/**
 * Created by danny on 3/11/16.
 */
public class TokenIndexReader {

    public TokenIndex read(String dir) throws IOException {

        TokenDictionary.Reader reader = new TokenDictionary.Reader(dir + File.separatorChar + TokenIndex.DICTIONARY_FILE);
        TokenDictionary tokenDictionary = reader.read();


        TokenInvertedIndex.Reader tokenIndexReader = new TokenInvertedIndex.Reader(dir + File.separatorChar + TokenIndex.INDEX_FILE);

        return new TokenIndex(tokenDictionary, tokenIndexReader);
    }
}
