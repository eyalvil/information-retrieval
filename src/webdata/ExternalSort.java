package webdata;

import webdata.utils.BufferedRandomAccessFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;

public class ExternalSort {

    private static final int MAX_BLOCK_SIZE = 1000000;

    private static final int MAX_NUMBER_BLOCKS = 8;

    private static final boolean DEBUG = false;

    private final File tempFile;

    private BufferedRandomAccessFile outputFile;

    private BufferedRandomAccessFile readFile;

    private int BLOCK_SIZE;

    private int NUMBER_BLOCKS;

    private double numberOfElements;

    private int totalBlocks;

    private long length;

    public ExternalSort(RandomAccessFile file, String dir) throws FileNotFoundException {

        this.readFile = new BufferedRandomAccessFile(file);
        tempFile = new File(dir + File.separatorChar + "tmp");
        this.outputFile = new BufferedRandomAccessFile(new RandomAccessFile(tempFile, "rw"));
    }

    public void close() {

        tempFile.delete();

    }

    public RandomAccessFile sort() throws IOException {

        initParams();

        initialSort();

        swapFiles();

        long diskBlockSize = 4 * 2 * BLOCK_SIZE;

        int end = (int) Math.ceil(Math.log(totalBlocks) / Math.log(NUMBER_BLOCKS));

        int iters = (int) Math.ceil((double) totalBlocks / NUMBER_BLOCKS);

        for (int pass = 1; pass <= end; pass++) {

            long blockPassSize = (long) ((Math.pow(NUMBER_BLOCKS, pass - 1)) * diskBlockSize);

            long cumSum = 0;

            for (int i = 0; i < iters; i++) {

                List<Long> ptrs = new ArrayList<>();

                for (int j = 0; j < NUMBER_BLOCKS; j++) {

                    ptrs.add(cumSum);

                    cumSum += blockPassSize;

                    if (cumSum >= length) {
                        break;
                    }

                }

                merge(ptrs, (int) Math.pow(NUMBER_BLOCKS, pass - 1));

            }

            swapFiles();

            iters = (int) Math.ceil((double) iters / NUMBER_BLOCKS);

        }


        RandomAccessFile res = readFile.getRandomAccessFile();

        // remove addiontal padded elements
        res.setLength(length);

        outputFile.close();
        readFile.close();

        return res;
    }


    private void swapFiles() throws IOException {

        BufferedRandomAccessFile tmp = this.outputFile;
        outputFile = this.readFile;
        readFile = tmp;

        outputFile.setLength(0);
        outputFile.seek(0);
        readFile.seek(0);

    }


    private void initialSort() throws IOException {

        readFile.seek(0);
        outputFile.seek(0);

        int count = 0;
        int numberOfElements1 = (int) numberOfElements;

        int max = 0;
        for (int i = 0; i < totalBlocks; i++) {

            List<Term> list = new ArrayList<>();
            int j;
            for (j = 0; j < BLOCK_SIZE && count < numberOfElements1; j++, count++) {
                int termId = readFile.readInt();
                int docId = readFile.readInt();
                max = Math.max(termId, max);
                list.add(Term.createTerm(termId, docId));
            }

            if (j < BLOCK_SIZE) {
                int termId = max + 1;
                for (; j < BLOCK_SIZE; j++) {
                    list.add(Term.createTerm(termId, 0));
                }
            }

            Collections.sort(list);

            for (Term term : list) {

                outputFile.writeInt(term.termId);

                outputFile.writeInt(term.docId);

            }

        }

        outputFile.flush();

    }

    private void initParams() throws IOException {

        length = readFile.length();

        numberOfElements = (double) readFile.length() / 8;

        // number of terms in file are n

        double candidate1 = Math.min(numberOfElements, MAX_BLOCK_SIZE);

        if (numberOfElements > MAX_BLOCK_SIZE) {
            NUMBER_BLOCKS = (int) Math.min(MAX_NUMBER_BLOCKS, numberOfElements / MAX_BLOCK_SIZE + 1);
            double candidate2 = Math.min(numberOfElements / NUMBER_BLOCKS, MAX_BLOCK_SIZE);
            BLOCK_SIZE = (int) candidate2;
        } else {
            NUMBER_BLOCKS = 1;
            BLOCK_SIZE = (int) candidate1;
        }

        totalBlocks = (int) Math.ceil(numberOfElements / BLOCK_SIZE);

        readFile.init(BLOCK_SIZE);
        outputFile.init(BLOCK_SIZE);

    }

    private void merge(List<Long> ptr, int pass) throws IOException {

        PriorityQueue<Block> priorityQueue = readBlocks(ptr);

        while (!priorityQueue.isEmpty()) {

            Block termBlockEntry = priorityQueue.poll();

            Term key = termBlockEntry.pollFirst();

            outputFile.writeInt(key.termId);
            outputFile.writeInt(key.docId);

            Term term = termBlockEntry.peekFirst();
            if (term != null) {

                priorityQueue.add(termBlockEntry);

            } else {

                int blockPass = termBlockEntry.count + 1;

                if (blockPass >= pass) {
                    continue;
                }

                Block block1 = null;
                try {
                    block1 = readBlock(termBlockEntry.ptrEnd, blockPass);
                } catch (IllegalStateException e) {
                    System.out.println(ptr);
                    throw new IllegalStateException();
                }
                if (block1 != null) {
                    priorityQueue.add(block1);
                }
            }
        }

        outputFile.flush();

    }

    private PriorityQueue<Block> readBlocks(List<Long> ptrs) throws IOException {


        PriorityQueue<Block> priorityQueue = new PriorityQueue<>();

        for (Long ptr : ptrs) {

            Block block = readBlock(ptr, 0);

            priorityQueue.add(block);

        }

        return priorityQueue;

    }

    private Block readBlock(Long ptr, int pass) throws IOException {

        if (ptr >= length) {
            return null;
        }

        readFile.seek(ptr);

        LinkedList<Term> terms = new LinkedList<>();

        for (int i = 0; i < BLOCK_SIZE; i++) {

            int termId = readFile.readInt();
            int docId = readFile.readInt();

            terms.add(Term.createTerm(termId, docId));

        }

        return new Block(readFile.getFilePointerBlockStart(), pass, terms);

    }


    private static class Block implements Comparable<Block> {

        private long ptrEnd;

        private int count;

        private LinkedList<Term> items;

        public Block(long ptrEnd, int count, LinkedList<Term> items) {
            this.ptrEnd = ptrEnd;
            this.count = count;
            this.items = items;
        }

        Term pollFirst() {

            return items.pollFirst();

        }

        Term peekFirst() {

            return items.peekFirst();

        }

        @Override
        public int compareTo(Block o) {

            Term term1 = items.peekFirst();
            Term term2 = o.items.peekFirst();

            int res = term1.compareTo(term2);

            if (res == 0) {


                return Long.compare(ptrEnd, o.ptrEnd);
            }

            return res;
        }
    }

    private static class Term implements Comparable<Term> {

        private int termId;

        private int docId;

        private Term(int termId, int docId) {
            this.termId = termId;
            this.docId = docId;
        }

        static Term createTerm(int termId, int docId) {
            return new Term(termId, docId);
        }

        @Override
        public int compareTo(Term o) {

            int compare = Integer.compare(termId, o.termId);

            if (compare == 0) {
                return Integer.compare(docId, o.docId);
            }

            return compare;

        }

        @Override
        public String toString() {
            return "Term{" +
                    "termId=" + termId +
                    ", docId=" + docId +
                    '}';
        }
    }

}
