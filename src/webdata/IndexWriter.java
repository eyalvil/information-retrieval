package webdata;

import webdata.product.ProductIndexBuilder;
import webdata.review.Review;
import webdata.review.ReviewIndexBuilder;
import webdata.token.TokenIndexBuilder;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by danny on 3/1/16.
 */
public class IndexWriter {


    public IndexWriter() {

    }

    /**
     * Given product review data, creates an on disk index
     * inputFile is the path to the file containing the review data
     * dir is the directory in which all index files will be created
     * if the directory does not exist, it should be created
     */
    public void write(String inputFile, String dir) {

        try {

            ReviewParser reviewParser = new ReviewParser(inputFile);

            File directory = new File(dir);
            if (!directory.isDirectory() && !directory.mkdirs()) {
                throw new IllegalStateException("Couldn't create directory");
            }

            ReviewIndexBuilder reviewIndexBuilder = new ReviewIndexBuilder(dir);

            TokenIndexBuilder tokenIndexBuilder = new TokenIndexBuilder(dir);

            ProductIndexBuilder productIndexBuilder = new ProductIndexBuilder();

            int reviewId = 0;

            Review review = reviewParser.nextReview();
            long end = 0 ;
            while (review != null) {
                reviewId++;

                reviewIndexBuilder.addTerm(review.getProductId(), review.getScore(), review.getHelpfulnessNumerator(), review.getHelpfulnessDenominator(), review.getTokenCount());

                List<String> tokens = review.getTokens();

                for (String token : tokens) {

                    tokenIndexBuilder.addTerm(token, reviewId);

                }

                productIndexBuilder.addProduct(review.getProductId(), reviewId);

                review = reviewParser.nextReview();
            }

            productIndexBuilder.write(dir);

            productIndexBuilder = null; // signal gc to free up this space

            tokenIndexBuilder.write();

            reviewIndexBuilder.close();

            reviewParser.close();

        } catch (IOException e) {

            e.printStackTrace();

        }
    }

    /**
     * Delete all index files by removing the given directory
     */
    public void removeIndex(String dir) {


        File file = new File(dir);

        File[] files = file.listFiles();

        for (File file1 : files) {
            file1.delete();
        }

        file.delete();

    }


}
