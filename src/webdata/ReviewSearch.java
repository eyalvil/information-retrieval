package webdata;

import webdata.search.LanguageModelRanker;
import webdata.search.ProductRanker;
import webdata.search.VectorSpaceRanker;

import java.util.Collection;
import java.util.Enumeration;

/**
 * Created by danny on 5/17/16.
 */
public class ReviewSearch {

    private final IndexReader iReader;

    /**
     * Constructor
     */
    public ReviewSearch(IndexReader iReader) {
        this.iReader = iReader;
    }

    /**
     * Returns a list of the id-s of the k most highly ranked reviews for the
     * given query, using the vector space ranking function lnn.ltc (using the
     * SMART notation)
     * The list should be sorted by the ranking
     */
    public Enumeration<Integer> vectorSpaceSearch(Enumeration<String> query, int k) {


        VectorSpaceRanker vectorSpaceRanker = new VectorSpaceRanker(iReader);
        return vectorSpaceRanker.getTopKReviews(query, k).elements();
    }

    /**
     * Returns a list of the id-s of the k most highly ranked reviews for the
     * given query, using the language model ranking function, smoothed using a
     * mixture model with the given value of lambda
     * The list should be sorted by the ranking
     */
    public Enumeration<Integer> languageModelSearch(Enumeration<String> query,
                                                    double lambda, int k) {

        LanguageModelRanker vectorSpaceRanking = new LanguageModelRanker(iReader, lambda);
        return vectorSpaceRanking.getTopKReviews(query, k).elements();

    }



//    /**
//     * Returns a list of the id-s of the k most highly ranked productIds for the
//     * given query using a function of your choice
//     * The list should be sorted by the ranking
//     */
      public Collection<String> productSearch(Enumeration<String> query, int k) {
          VectorSpaceRanker vectorSpaceRanker = new VectorSpaceRanker(iReader);
          ProductRanker productRanker = new ProductRanker(iReader, vectorSpaceRanker);
          return productRanker.getTopKReviews(query, k);
      }
}
