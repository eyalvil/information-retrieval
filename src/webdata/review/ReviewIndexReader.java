package webdata.review;

import java.io.File;
import java.io.IOException;

/**
 * Created by eyal on 13/03/2016.
 */
public class ReviewIndexReader {

    public static final String REVIEW_DICT = "review.dict";

    public ReviewIndexReader() {

    }

    public ReviewIndex read(String dir) throws IOException {

        ReviewDictionary.Reader reader =  new ReviewDictionary.Reader(dir + File.separatorChar + REVIEW_DICT);

        return new ReviewIndex(reader);
    }
}
