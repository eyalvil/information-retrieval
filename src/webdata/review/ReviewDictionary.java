package webdata.review;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by eyal on 13/03/2016.
 */
class ReviewDictionary {

    static class ReviewMetaData {

        String productId;

        short score;

        int helpfullnessNum;

        int helpfullnessDenum;

        int length;

        ReviewMetaData(String productId, short score, int help1, int help2, int length) {

            this.productId = productId;
            this.score = score;
            this.helpfullnessNum = help1;
            this.helpfullnessDenum = help2;
            this.length = length;
        }
    }

    static class Writer {

        private final DataOutputStream file;

        public Writer(String file) throws FileNotFoundException {

            this.file = new DataOutputStream(new FileOutputStream(file));

        }

        public void write(List<ReviewMetaData> reviews) throws IOException {

            ByteBuffer byteBuffer = ByteBuffer.allocate(reviews.size() * reviewSize());

            for (ReviewMetaData review : reviews) {

                byte[] bytes = review.productId.getBytes();

                byteBuffer.put(bytes);
                byteBuffer.putShort(review.score);
                byteBuffer.putInt(review.helpfullnessNum);
                byteBuffer.putInt(review.helpfullnessDenum);
                byteBuffer.putInt(review.length);

            }

            file.write(byteBuffer.array());


        }

        public void close() throws IOException {

            file.close();
        }

    }

    static class Reader {

        private final RandomAccessFile file;

        public Reader(String file) throws FileNotFoundException {

            this.file = new RandomAccessFile(new File(file), "r");
        }

        public ReviewMetaData read(int reviewIndex) throws IOException {

            if (reviewIndex <= 0) {
                return null;
            }

            long position = (reviewIndex - 1) * reviewSize();

            if (file.length() <= position) {
                return null;
            }

            file.seek(position);


            byte[] str = new byte[10];
            file.read(str);

            String productId = new String(str);

            short score = file.readShort();
            int help1 = file.readInt();
            int help2 = file.readInt();
            int length = file.readInt();

            return new ReviewMetaData(productId, score, help1, help2, length);

        }

        public int size() throws IOException {
            return (int) (file.length() / reviewSize());
        }

    }

    private static int reviewSize() {
        return 2 + 4 + 4 + 4 + 10;
    }

    static class Builder {

        private final Writer writer;

        private List<ReviewMetaData> reviews = new ArrayList<>();

        private int MAX_REVIEW = 50000;

        Builder(String file) throws FileNotFoundException {
            writer = new Writer(file);
        }

        void addReview(String prodId, short score, int help1, int help2, int length) throws IOException {

            reviews.add(new ReviewMetaData(prodId, score, help1, help2, length));

            if (reviews.size() > MAX_REVIEW) {
                writer.write(reviews);
                reviews.clear();

            }

        }

        void close() throws IOException {

            if (!reviews.isEmpty()) {
                writer.write(reviews);
            }

            reviews = null;

            writer.close();

        }


    }
}
