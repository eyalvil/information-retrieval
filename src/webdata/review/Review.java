package webdata.review;

import java.util.List;

/**
 * Created by danny on 3/1/16.
 */
public class Review {

    private String productId;

    private int helpfulnessNumerator;

    private int helpfulnessDenominator;

    private short score;

    private int tokenCount;

    private List<String> tokens;

    public Review(String productId, int helpfulnessNumerator, int helpfulnessDenominator, short score, int tokenCount, List<String> tokens) {
        this.productId = productId;
        this.helpfulnessNumerator = helpfulnessNumerator;
        this.helpfulnessDenominator = helpfulnessDenominator;
        this.score = score;
        this.tokenCount = tokenCount;
        this.tokens = tokens;
    }

    public String getProductId() {
        return productId;
    }

    public int getHelpfulnessNumerator() {
        return helpfulnessNumerator;
    }

    public int getHelpfulnessDenominator() {
        return helpfulnessDenominator;
    }

    public int getTokenCount() {
        return tokenCount;
    }

    public static ReviewBuilder builder() {
        return new ReviewBuilder();
    }

    public short getScore() {
        return score;
    }

    public List<String> getTokens() {
        return tokens;
    }

    public static class ReviewBuilder {

        private String productId;
        private int helpfulnessNumerator;
        private int helpfulnessDenominator;
        private short score;
        private int tokenNumbers;
        private List<String> tokens;

        private ReviewBuilder() {
        }

        public ReviewBuilder setProductId(String productId) {
            this.productId = productId;
            return this;
        }

        public ReviewBuilder setHelpfulnessNumerator(int helpfulnessNumerator) {
            this.helpfulnessNumerator = helpfulnessNumerator;
            return this;
        }

        public ReviewBuilder setHelpfulnessDenominator(int helpfulnessDenominator) {
            this.helpfulnessDenominator = helpfulnessDenominator;
            return this;
        }

        public ReviewBuilder setTokenCount(int tokenNumbers) {
            this.tokenNumbers = tokenNumbers;
            return this;
        }

        public ReviewBuilder setScore(short score) {
            this.score = score;
            return this;
        }

        public void setTokens(List<String> tokens) {
            this.tokens = tokens;
        }
        public Review createReview() {
            return new Review(productId, helpfulnessNumerator, helpfulnessDenominator, score, tokenNumbers, tokens);

        }
    }
}
