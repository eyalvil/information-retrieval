package webdata.review;

import java.io.IOException;

/**
 * Created by eyal on 13/03/2016.
 */
public class ReviewIndex {

    static final String DICTIONARY_FILE = "review.dict";
    private final ReviewDictionary.Reader reviewReader;


    ReviewIndex(ReviewDictionary.Reader reviewReader) {

        this.reviewReader = reviewReader;

    }

    public String getProductId(int revId) throws IOException {
        ReviewDictionary.ReviewMetaData metaData = reviewReader.read(revId);

        if (metaData != null) {
            return metaData.productId;
        } else {
            return null;
        }

    }

    public int getReviewScore(int revId) throws IOException {
        ReviewDictionary.ReviewMetaData metaData = reviewReader.read(revId);

        if (metaData != null) {
            return (int) metaData.score;
        } else {
            return -1;
        }

    }

    public int getReviewHelpfulnessNumerator(int revId) throws IOException {
        ReviewDictionary.ReviewMetaData metaData = reviewReader.read(revId);

        if (metaData != null) {
            return metaData.helpfullnessNum;
        } else {
            return -1;
        }

    }

    public int getReviewHelpfulnessDenominator(int revId) throws IOException {
        ReviewDictionary.ReviewMetaData metaData = reviewReader.read(revId);
        if (metaData != null) {

            return metaData.helpfullnessDenum;
        } else {
            return -1;
        }
    }

    public int getReviewLength(int revId) throws IOException {

        ReviewDictionary.ReviewMetaData metaData = reviewReader.read(revId);
        if (metaData != null) {
            return metaData.length;
        } else {
            return -1;
        }
    }

    public int size() throws IOException {

        return reviewReader.size();

    }
}
