package webdata.review;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by eyal on 13/03/2016.
 */
public class ReviewIndexBuilder {


    private ReviewDictionary.Builder dictionaryBuilder;

    public ReviewIndexBuilder(String dir) throws FileNotFoundException {

        dictionaryBuilder = new ReviewDictionary.Builder(dir + File.separatorChar + ReviewIndex.DICTIONARY_FILE);

    }

    public void addTerm(String prodId, short score, int help1, int help2, int length) throws IOException {

        dictionaryBuilder.addReview(prodId, score, help1, help2, length);


    }


    public void close() throws IOException {


        dictionaryBuilder.close();
    }
}
